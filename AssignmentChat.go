package main

import (
	"fmt"
)

func mainChat() {
	angka := []float32{4.0, 4.0, 5.0, 4.0}
	hasil := getAverage(angka)

	fmt.Printf("Rata-rata = %.2f", hasil)
}

func getAverage(arr []float32) float32 {
	var sum float32
	for _, element := range arr {
		sum += element
	}
	return sum / float32(len(arr))
}
