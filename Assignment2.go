package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main2() {
	scanner := bufio.NewScanner(os.Stdin)
	for {
		fmt.Printf("\n")
		fmt.Println(strings.Repeat("-", 20))
		fmt.Println("\tMENU")
		fmt.Println(strings.Repeat("-", 20))
		fmt.Println("1. Buat Array 1 dimensi")
		fmt.Println("2. Buat Array 2 dimensi")
		fmt.Println("3. Exit")

		fmt.Printf("Masukan Input: ")
		scanner.Scan()
		userInput, _ := strconv.ParseInt(scanner.Text(), 10, 64)

		if userInput == 3 {
			fmt.Printf("Program Exited")
			break
		}
		switch userInput {
		case 1:
			// array 1 dimensi
			fmt.Print("Masukan jumlah array: ")
			scanner.Scan()
			jumlah, _ := strconv.ParseInt(scanner.Text(), 10, 64)

			// fmt.Scanln(&len)
			input := make([]int64, jumlah)

			for i := 0; i < int(jumlah); i++ {
				fmt.Print("Masukan angka: ")
				scanner.Scan()
				angka, _ := strconv.ParseInt(scanner.Text(), 10, 64)
				input[i] = angka
			}
			fmt.Println(input)

		case 2:
			fmt.Print("Masukan jumlah baris: ")
			scanner.Scan()
			row, _ := strconv.ParseInt(scanner.Text(), 10, 64)
			fmt.Print("Masukan jumlah kolom: ")
			scanner.Scan()
			col, _ := strconv.ParseInt(scanner.Text(), 10, 64)
			input := make([][]int64, int(row))
			for i := 0; i < int(row); i++ {
				input[i] = make([]int64, int(col))
				for j := 0; j < int(col); j++ {
					fmt.Printf("Masukan angka: [%d][%d]: ", i, j)
					scanner.Scan()
					angka, err := strconv.ParseInt(scanner.Text(), 10, 64)
					if err != nil {
						fmt.Println(err)
					}
					input[i][j] = angka
				}
			}
			fmt.Println(input)

		}

	}
}
