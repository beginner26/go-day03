package main

import (
	"bufio"
	"fmt"
	"naufal/calculate"
	"os"
	"strconv"
	"strings"
)

func mainExtra() {
	scanner := bufio.NewScanner(os.Stdin)

	for {
		fmt.Printf("\n")
		fmt.Println(strings.Repeat("-", 20))
		fmt.Println("\tMENU")
		fmt.Println(strings.Repeat("-", 20))
		fmt.Println("1. Hitung Luas Segitiga")
		fmt.Println("2. Hitung Luas Lingkaran")
		fmt.Println("3. Hitung Luas Persegi")
		fmt.Println("4. Exit")

		fmt.Printf("Masukan Input: ")
		scanner.Scan()
		userInput, _ := strconv.ParseInt(scanner.Text(), 10, 64)

		if userInput == 4 {
			fmt.Printf("Program Exited")
			break
		}

		switch userInput {
		case 1:
			fmt.Println("Menghitung luas Segitiga")
			fmt.Print("Masukan alas: ")
			scanner.Scan()
			a, _ := strconv.ParseFloat(scanner.Text(), 10)

			fmt.Print("Masukan tinggi: ")
			scanner.Scan()
			t, _ := strconv.ParseFloat(scanner.Text(), 10)

			segitiga := calculate.Segitiga{
				Alas:   a,
				Tinggi: t,
			}
			fmt.Printf("Luas segitiga adalah: %.2f\n", segitiga.GetLuas())
		case 2:
			fmt.Println("Menghitung luas Lingkaran")

			fmt.Printf("Masukan jari-jari: ")
			scanner.Scan()
			r, _ := strconv.ParseFloat(scanner.Text(), 10)

			lingkaran := calculate.Lingkaran{
				JariJari: r,
			}

			fmt.Printf("Luas lingkaran adalah %.2f", lingkaran.GetLuas())
		case 3:
			fmt.Println("Menghitung luas Persegi")

			fmt.Printf("Masukan panjang: ")
			scanner.Scan()
			p, _ := strconv.ParseFloat(scanner.Text(), 10)

			fmt.Printf("Masukan lebar: ")
			scanner.Scan()
			l, _ := strconv.ParseFloat(scanner.Text(), 10)

			persegi := calculate.Persegi{
				Panjang: p,
				Lebar:   l,
			}
			fmt.Printf("Luas persegi adalah %.2f", persegi.GetLuas())
		}
	}
}
