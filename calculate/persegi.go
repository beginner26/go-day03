package calculate

type Persegi struct {
	Panjang float64
	Lebar   float64
}

func (p Persegi) GetLuas() float64 {
	return p.Panjang * p.Lebar
}
