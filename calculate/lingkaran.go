package calculate

import "math"

type Lingkaran struct {
	JariJari float64
}

func (l Lingkaran) GetLuas() float64 {

	return math.Pi * l.JariJari * l.JariJari
}
