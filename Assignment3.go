package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Print("Masukan jumlah yang ingin di input: ")
	scanner.Scan()
	jumlah, _ := strconv.ParseInt(scanner.Text(), 10, 64)
	words := make([]string, jumlah)

	for i := 0; i < int(jumlah); i++ {
		fmt.Print("Masukan kata: ")
		scanner.Scan()
		word := scanner.Text()
		words[i] = word
	}

	fmt.Println(words)

	var filtered []string

	fmt.Print("Masukan yang ingin dicari: ")
	scanner.Scan()
	cari := scanner.Text()
	for _, el := range words {
		// fmt.Println(el)
		if strings.Contains(el, cari) {
			filtered = append(filtered, el)
		}
	}

	for i, el := range filtered {
		fmt.Printf("Hasil %d: %s \n", i+1, el)
	}
}
