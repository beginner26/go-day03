package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
)

func main1() {
	scanner := bufio.NewScanner(os.Stdin)
	for {
		fmt.Printf("\n")
		fmt.Println(strings.Repeat("-", 20))
		fmt.Println("\tMENU")
		fmt.Println(strings.Repeat("-", 20))
		fmt.Println("1. Hitung Volume Kubus")
		fmt.Println("2. Hitung Volume Bola")
		fmt.Println("3. Hitung Umur")
		fmt.Println("4. Exit")

		fmt.Printf("Masukan Input: ")
		scanner.Scan()
		userInput, _ := strconv.ParseInt(scanner.Text(), 10, 64)

		if userInput == 4 {
			fmt.Printf("Program Exited")
			break
		}
		switch userInput {
		case 1:
			fmt.Println("Menghitung volume kubus")
			fmt.Print("Masukan panjang rusuk: ")
			scanner.Scan()
			s, _ := strconv.ParseFloat(scanner.Text(), 10)
			fmt.Printf("Hasil Volume Kubus: %2.f", hitungVolumeKubus(s))
		case 2:
			fmt.Println("Menghitung volume bola")
			fmt.Print("Masukan jari-jari: ")
			scanner.Scan()
			r, _ := strconv.ParseFloat(scanner.Text(), 10)
			fmt.Printf("Hasil Volume Bola: %2.f", hitungVolumeBola(r))
		case 3:
			fmt.Println("Menghitung umur")
			fmt.Print("Masukan tahun sekarang: ")
			scanner.Scan()
			s, _ := strconv.ParseInt(scanner.Text(), 10, 64)
			fmt.Print("Masukan tahun lahir: ")
			scanner.Scan()
			l, _ := strconv.ParseInt(scanner.Text(), 10, 64)
			fmt.Printf("Hasil menghitung umur: %d", hitungUmur(s, l))
		}
	}
}

func hitungVolumeKubus(s float64) float64 {
	return s * s * s
}

func hitungVolumeBola(r float64) float64 {
	return (4 / 3) * math.Pi * (r * r * r)
}

func hitungUmur(tahunSekarang, tahunLahir int64) int64 {
	return tahunSekarang - tahunLahir
}
